#coding: utf-8
import os
import hashlib
from suds.client import Client


class BaseWS(object):
    """
    WebService Base class to connect with cfe services.
    All others must heritate from this
    """
    # This are hardcoded for testing purposes
    UsuarioMovil = 'usrReto071'
    PasswordMovil = os.environ.get('WS_PASSWORD', None)
    SistemaOperativo = 'iOS'
    VersionSO = '8.1'
    TipoEquipo = 'IPhone'
    ModeloEquipo = '6'
    IP = "208.24.13.5"
    LlavePrivada = os.environ.get('WS_PRIVATE_KEY', None)

    def __init__(self):
        self.client = Client("http://aplicaciones.cfe.gob.mx/WebServices/CFEMovil/CFEMovil.svc?wsdl")

    def getAccess(self):
        access = self.client.factory.create('ns3:AccesoCliente')
        access.Hash = None
        access.Ip = self.IP
        access.ModeloEquipo = self.ModeloEquipo
        access.PasswordMovil = self.PasswordMovil
        access.ResolucionEquipo = None
        access.SistemaOperativo = self.SistemaOperativo
        access.TipoEquipo = self.TipoEquipo
        access.Ubicacion = None
        access.UsuarioMovil = self.UsuarioMovil
        access.VersionSO = self.VersionSO
        return access

    def createHash(self, elements=[]):
        # Campos Obligatorios Seguridad:
        _str = "%s%s%s%s%s%s%s" % (self.UsuarioMovil,
                                   self.PasswordMovil,
                                   self.SistemaOperativo,
                                   self.VersionSO,
                                   self.TipoEquipo,
                                   self.ModeloEquipo,
                                   self.IP)
        # + Campos Obligatorios Procesamiento:
        for item in elements:
            _str += item
        # + LlavePrivada = HASH
        _str += self.LlavePrivada
        return hashlib.sha1(_str).hexdigest()


class ObtenerEstados(BaseWS):
    """
    Método que regresa una lista de los Estados del país
    """
    def execute(self):
        acceso = self.getAccess()
        acceso.Hash = self.createHash()
        return self.client.service.ObtenerEstados(acceso)


class AgenciasPorEstado(BaseWS):
    """
    Método que regresa información de las agencias que existen en deteminado Estado
    """
    def __init__(self, id_estado=21, rpu="972840600035"):
        super(AgenciasPorEstado, self).__init__()
        self.id_estado = str(id_estado)
        self.rpu = rpu

    def execute(self):
        acceso = self.getAccess()
        acceso.Hash = self.createHash([self.id_estado, self.rpu])
        return self.client.service.AgenciasPorEstado(acceso, self.id_estado, self.rpu)


class SELAclaraRecibo(BaseWS):
    """
    Método que registra una solicitud de Aclaración de Recibo
    """
    def __init__(self, rpu="972840600035", correo="test@test.com", observaciones="test"):
        super(SELAclaraRecibo, self).__init__()
        self.correo = correo
        self.rpu = rpu
        self.observaciones = observaciones

    def execute(self):
        acceso = self.getAccess()
        acceso.Hash = self.createHash([self.rpu, self.correo, self.observaciones])
        return self.client.service.SELAclaraRecibo(acceso,
                                                   self.rpu,
                                                   self.correo,
                                                   self.observaciones)
