#coding: utf-8
from django.db import models
from contracts.models import Contract


class Report(models.Model):
    """  """
    user = models.ForeignKey('users.CfeUser')
    contract = models.ForeignKey(Contract)
    category = models.ForeignKey('ReportType', verbose_name='Tipo de reporte')
    image = models.ImageField('imagen', blank=True, upload_to='reports')
    latitude = models.FloatField("Latitud", blank=True, null=True)
    longitude = models.FloatField("Longitud", blank=True, null=True)
    comments = models.TextField("Observaciones")
    status = models.ForeignKey('StatusType')
    is_active = models.BooleanField(default=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'reporte'

    def __unicode__(self):
        return "%s" % self.category.name


class Status(models.Model):
    """ Status of report. """
    status_type = models.ForeignKey('StatusType')
    comment = models.CharField(max_length=150)
    is_active = models.BooleanField(default=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name_plural = 'Status'

    def __unicode__(self):
        return self.status_type.name


class ReportType(models.Model):
    """ Type of a report can be """
    name = models.CharField(max_length=50)
    is_active = models.BooleanField(default=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'Tipo de reporte'
        verbose_name_plural = 'Tipos de reporte'

    def __unicode__(self):
        return self.name


class StatusType(models.Model):
    """ Catalog of type of status a report can have """
    name = models.CharField(max_length=50)
    is_active = models.BooleanField(default=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'Tipo de Status'
        verbose_name_plural = 'Tipos de Status'

    def __unicode__(self):
        return self.name
