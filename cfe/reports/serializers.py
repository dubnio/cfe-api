#coding: utf-8
from rest_framework import serializers
from .models import Report


class ReportSerializer(serializers.ModelSerializer):
    user = serializers.Field(source='user.username')

    class Meta:
        model = Report
