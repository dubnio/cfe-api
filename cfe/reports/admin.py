#coding: utf-8
from django.contrib import admin
from .models import(
    Report,
    ReportType,
    Status,
    StatusType
)


class ReportAdmin(admin.ModelAdmin):
    list_display = ('category', 'user', 'status', 'created_date', 'is_active')
    list_filter = ('status', 'category')
    search_fields = ('user__name', 'comments')

admin.site.register(Report, ReportAdmin)
admin.site.register(ReportType)
admin.site.register(Status)
admin.site.register(StatusType)
