#coding: utf-8
from .models import Report, Status
from .serializers import ReportSerializer
from rest_framework import viewsets


class ReportViewSet(viewsets.ModelViewSet):
    model = Report
    serializer_class = ReportSerializer

    def get_queryset(self):
        try:
            queryset = Report.objects.filter(
                user=self.request.user
            )
        except:
            queryset = Report.objects.all()
        return queryset

    def pre_save(self, obj):
        obj.user = self.request.user


class StatusViewSet(viewsets.ModelViewSet):
    model = Status
