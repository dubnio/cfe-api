#coding: utf-8
from .models import(
    Notification,
)

from rest_framework import viewsets


class NotificationViewSet(viewsets.ModelViewSet):
    model = Notification
