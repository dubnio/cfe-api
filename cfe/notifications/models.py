#coding: utf-8
from django.db import models


class Notification(models.Model):
    """ Push notifications register """
    to = models.ManyToManyField('users.CfeUser', verbose_name='destinatario(s)')
    title = models.CharField('título', max_length='50')
    message = models.CharField('mensaje', max_length='100')
    notification_type = models.ForeignKey('NotificationType',
                                          verbose_name='tipo de notificación')
    report = models.ForeignKey('reports.Report', blank=True, null=True)
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'Notificación'
        verbose_name_plural = 'Notificaciones'

    def __unicode__(self):
        return self.title


class NotificationType(models.Model):
    """ Type notication can be Alert, Info or Report has changed its status """
    name = models.CharField(max_length=100)
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'Tipo de notificación'
        verbose_name_plural = 'Tipos de notificación'

    def __unicode__(self):
        return self.name
