#coding: utf-8
from django.db import models


class Contract(models.Model):
    """ Light contract of a user """
    user = models.ForeignKey('users.CfeUser')
    name = models.CharField('Nombre', max_length=30)
    number = models.BigIntegerField('Número de servicio', max_length=12, unique=True)
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'Contrato'

    def __unicode__(self):
        return "%s" % self.number


class Receipt(models.Model):
    """ Bimonthly receipt of a Contract """
    contract = models.ForeignKey('Contract')
    consuption = models.IntegerField('Consumo kWh', default=208)
    reading = models.IntegerField('Lectura Actual')
    total = models.FloatField('Total a pagar')
    limit_date = models.DateField('fecha límite', blank=True, null=True)
    due_date = models.DateField('fecha de corte', blank=True, null=True)
    payment_date = models.DateField('fecha de pago', blank=True, null=True)
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'Recibo'

    def __unicode__(self):
        return "%s" % self.id


class Use(models.Model):
    """ Defines type use of light ex. commertial, domestic, .."""
    name = models.CharField('Tipo de uso', max_length=50)
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'Uso'

    def __unicode__(self):
        return self.name


class Rate(models.Model):
    """ Code of rate applied """
    code = models.CharField('Clave de tarifa', max_length=2)
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'tarifa'

    def __unicode__(self):
        return self.code
