#coding: utf-8
from django.contrib import admin
from .models import(
    Contract,
    Receipt,
    Use,
    Rate
)


class ContractAdmin(admin.ModelAdmin):
    list_display = ('name', 'user', 'number', 'created_at', 'is_active')
    list_filter = ('user',)
    search_fields = ('user__email', 'number')

admin.site.register(Contract, ContractAdmin)
admin.site.register(Receipt)
admin.site.register(Use)
admin.site.register(Rate)
