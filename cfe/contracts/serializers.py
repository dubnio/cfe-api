#coding: utf-8
from rest_framework import serializers
from .models import Contract


class ContractSerializer(serializers.ModelSerializer):
    user = serializers.Field(source='user.username')

    # def validate_number(self, attrs, source):
    #     """
    #     Check the number of contract exists  This service is not available
    #     """
    #     value = attrs[source]
    #     return attrs

    class Meta:
        model = Contract
