#coding: utf-8
from .models import(
    Contract,
    Receipt
)

from rest_framework import viewsets
from contracts.serializers import ContractSerializer


class ContractViewSet(viewsets.ModelViewSet):
    model = Contract
    serializer_class = ContractSerializer

    def get_queryset(self):
        try:
            queryset = Contract.objects.filter(
                user=self.request.user
            )
        except:
            queryset = Contract.objects.all()
        return queryset

    def pre_save(self, obj):
        obj.user = self.request.user


class ReceiptViewSet(viewsets.ModelViewSet):
    model = Receipt
