#coding: utf-8
from .models import(
    News
)

from rest_framework import viewsets


class NewsViewSet(viewsets.ModelViewSet):
    model = News
