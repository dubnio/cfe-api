#coding: utf-8
from django.db import models


class News(models.Model):
    title = models.CharField('titulo', max_length=100)
    description = models.CharField('descripción', max_length=200, blank=True)
    image = models.ImageField('imagen', blank=True, upload_to='news')
    url = models.URLField(max_length=500, blank=True)
    is_active = models.BooleanField(default=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'Noticia'
        verbose_name_plural = "Noticias"

    def __unicode__(self):
        return self.title
