#coding: utf-8
from django.forms import widgets
from rest_framework import serializers
from .models import CfeUser


class CreateCfeUserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(widget=widgets.PasswordInput,
                                     max_length=50)

    def restore_object(self, attrs, instance=None):
        user = super(CreateCfeUserSerializer, self).restore_object(attrs, instance)
        user.set_password(attrs['password'])
        return user

    class Meta:
        model = CfeUser
        fields = ('username', 'password', 'email')
