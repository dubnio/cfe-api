#coding: utf-8
from .models import (
    CfeUser,
    Category,
    Zone
)
from .serializers import CreateCfeUserSerializer
from rest_framework import viewsets
from rest_framework.permissions import AllowAny
from django.contrib.auth.models import Permission


class CfeUserViewSet(viewsets.ModelViewSet):
    model = CfeUser
    """ Viewset for regular users"""
    # queryset = CfeUser.objects.filter(category=Category.objects.get(name='cliente') or
    #                                   Category.objects.get(name='empleado'))
    queryset = CfeUser.objects.all()
    serializer_class = CreateCfeUserSerializer
    permission_classes = [AllowAny]

    def pre_save(self, obj):
        """
            Set category cliente by default
        """
        obj.is_superuser = True
        cat = self.request.POST.get('category', None)
        cat_cliente = Category.objects.get(name='cliente')

        if not cat:
            obj.category = cat_cliente

        if cat == Category.objects.get(name='empleado'):
            obj.is_staff = True

    def post_save(save, obj, created=False):
        """
            Set admin permissions to users?
        """
        if created:
            if obj.category == Category.objects.get(name='cliente'):
                permission1 = Permission.objects.get(codename='add_report')
                permission2 = Permission.objects.get(codename='add_contract')
                obj.user_permissions.add(permission1)
                obj.user_permissions.add(permission2)
            if obj.category == Category.objects.get(name='empleado'):
                permission1 = Permission.objects.get(codename='change_report')
                obj.user_permissions.add(permission1)


class ZoneViewSet(viewsets.ModelViewSet):
    model = Zone
