#coding: utf-8
import re
from django.db import models
from django.db.models.signals import post_save
from django.core import validators
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.dispatch import receiver
from django.contrib.auth.models import (
    BaseUserManager,
    AbstractBaseUser,
    PermissionsMixin,
)
from rest_framework.authtoken.models import Token


class CfeUserManager(BaseUserManager):
    """
    Manager to create new users with the given username, email and password.
    """
    def _create_user(self, username, email, password,
                     is_staff, is_superuser, **extra_fields):
        now = timezone.now()
        if not username:
            raise ValueError('The given username must be set')
        email = self.normalize_email(email)
        user = self.model(username=username, email=email,
                          is_staff=True, is_active=True,
                          is_superuser=True, last_login=now,
                          created_at=now, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, username, email=None, password=None, **extra_fields):
        return self._create_user(username, email, password, False, False,
                                 **extra_fields)

    def create_superuser(self, username, email, password, **extra_fields):
        return self._create_user(username, email, password, True, True,
                                 **extra_fields)


class CfeUser(AbstractBaseUser, PermissionsMixin):
    """
    Custom User Definition. (AbstractUser extended)
    Username, password and email are required. Other fields are optional.
    """
    username_regex = re.compile('^[\w.@+-]+$')
    username = models.CharField(_('username'), max_length=30, unique=True,
                                help_text=_('Required. 30 characters or fewer. Letters, \
                                            numbers and @/./+/-/_ characters'),
                                validators=[validators.RegexValidator(username_regex,
                                            _('Enter a valid username.'), 'invalid')]
                                )
    first_name = models.CharField(_('first name'), max_length=30, blank=True)
    last_name = models.CharField(_('last name'), max_length=30, blank=True)
    email = models.EmailField(_('email address'), unique=True)
    is_staff = models.BooleanField(_('staff status'), default=False,
                                   help_text=_('Designates whether the user can log \
                                                into this admin site.')
                                   )
    zone = models.ForeignKey('Zone', blank=True, null=True,
                             help_text='Geographical zone assigned to user')
    category = models.ForeignKey('Category', blank=True, null=True,
                                 help_text='Profile of user')
    address = models.CharField(_('address'), blank=True, max_length=100)
    employee_number = models.CharField('número de empleado', max_length=8, blank=True)
    is_active = models.BooleanField(_('active'), default=True,
                                    help_text=_('Designates whether this user should \
                                                be treated as active. Unselect this \
                                                instead of deleting accounts.')
                                    )
    created_at = models.DateTimeField('fecha de creación', auto_now_add=True)
    updated_at = models.DateTimeField('fecha de actualización', auto_now=True)

    objects = CfeUserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def __unicode__(self):
        return self.email

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        "Returns the short name for the user."
        return self.first_name


class Zone(models.Model):
    """
    Geographical areas Catalog
    """
    name = models.CharField(_('name'), max_length=30)
    is_active = models.BooleanField(_('active'), default=True)
    created_at = models.DateTimeField('fecha de creación', auto_now_add=True)
    updated_at = models.DateTimeField('fecha de actualización', auto_now=True)

    class Meta:
        verbose_name = 'Zona'
        ordering = ('id', )

    def __unicode__(self):
        return self.name


class Category(models.Model):
    """
    Types of user Catalog
    """
    name = models.CharField(_('name'), max_length=30)
    is_active = models.BooleanField(_('active'), default=True)
    created_at = models.DateTimeField('fecha de creación', auto_now_add=True)
    updated_at = models.DateTimeField('fecha de actualización', auto_now=True)

    class Meta:
        verbose_name = 'Categoría'
        ordering = ('id', )

    def __unicode__(self):
        return self.name


@receiver(post_save, sender=CfeUser)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)
