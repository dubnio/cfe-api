#coding utf-8
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .models import(
    CfeUser,
    Category,
    Zone
)
from .forms import(
    UserChangeForm,
    UserCreationForm
)


class CfeUserAdmin(UserAdmin):
    form = UserChangeForm
    add_form = UserCreationForm
    list_display = ('email', 'username', 'first_name', 'last_name', 'category', 'is_active')
    list_filter = ('is_superuser',)
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        ('Personal info', {'fields': ('first_name', 'last_name', 'category')}),
        ('Dates', {'fields': ('last_login',)}),
        ('Other', {'fields': ('zone',)}),
        ('Permissions', {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),

    )
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2')}
        ),
    )
    search_fields = ('email',)
    ordering = ('email',)
    filter_horizontal = ()


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'is_active')
    list_editable = ('is_active',)


class ZoneAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'is_active')
    list_editable = ('is_active',)

admin.site.register(CfeUser, CfeUserAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Zone, ZoneAdmin)
