from common import *

INSTALLED_APPS += (
    'debug_toolbar',
    'django_extensions',
    'utils'
)

INTERNAL_IPS = ('127.0.0.1', '33.33.0.1')

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'cfe',
        'USER': 'root',
        'PASSWORD': 'root',
        'HOST': '',
        'PORT': '',
    }
}

MIDDLEWARE_CLASSES += (
    'debug_toolbar.middleware.DebugToolbarMiddleware',
)

DEBUG_TOOLBAR_PATCH_SETTINGS = False
DEBUG_TOOLBAR_CONFIG = {
    'INTERCEPT_REDIRECTS': False,
    'SHOW_TEMPLATE_CONTEXT': True,
}
#Emails aren't send but displayed in console
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
