#Heroku settings
from common import *

INSTALLED_APPS += (
    'debug_toolbar',
    'django_extensions',
    'gunicorn',
)

INTERNAL_IPS = ('127.0.0.1', '33.33.0.1')

MIDDLEWARE_CLASSES += (
    'debug_toolbar.middleware.DebugToolbarMiddleware',
)

DEBUG_TOOLBAR_PATCH_SETTINGS = False
DEBUG_TOOLBAR_CONFIG = {
    'INTERCEPT_REDIRECTS': False,
    'SHOW_TEMPLATE_CONTEXT': True,
}
#Emails aren't send but displayed in console
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

DEBUG = True
TEMPLATE_DEBUG = DEBUG

# Parse database configuration from $DATABASE_URL
import dj_database_url
DATABASES['default'] = dj_database_url.config()

# Honor the 'X-Forwarded-Proto' header for request.is_secure()
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

# Allow all host headers
ALLOWED_HOSTS = ['*']

# Static asset configuration

STATIC_ROOT = 'staticfiles'
STATIC_URL = '/static/'
