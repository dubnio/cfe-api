#coding: utf-8
from django.shortcuts import redirect


def afterLogin(request):
    """ After login redirect to api again. """
    return redirect('/api/')
