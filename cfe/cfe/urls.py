from django.conf.urls import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.views.generic import TemplateView
from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin

from rest_framework.routers import DefaultRouter
from rest_framework.authtoken import views

from users import views as user_views
from contracts import views as contract_views
from reports import views as reports_views
from news import views as news_views
from notifications import views as notifications_views
from cacs import views as cac_views

admin.autodiscover()
router = DefaultRouter()

router.register(r'users', user_views.CfeUserViewSet)
router.register(r'contracts', contract_views.ContractViewSet)
router.register(r'recipts', contract_views.ReceiptViewSet)

router.register(r'reports', reports_views.ReportViewSet)
router.register(r'news', news_views.NewsViewSet)
router.register(r'notifications', notifications_views.NotificationViewSet)
router.register(r'cac', cac_views.CacViewSet)

urlpatterns = patterns(
    '',
    url(r'^$', TemplateView.as_view(template_name='home.html')),
    url(r'^api/', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^accounts/profile/$', 'cfe.views.afterLogin'),
    url(r'^admin/dashboard', TemplateView.as_view(template_name='admin/dashboard.html')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^404/$', TemplateView.as_view(template_name='404.html')),
    url(r'^500/$', TemplateView.as_view(template_name='500.html')),
)

urlpatterns += [
    url(r'^api/tokenauth/', views.obtain_auth_token)
]

if settings.DEBUG:
    urlpatterns += staticfiles_urlpatterns() + static(
        settings.MEDIA_URL, document_root=settings.MEDIA_ROOT
    )
    import debug_toolbar
    urlpatterns += patterns('',
        url(r'^__debug__/', include(debug_toolbar.urls)),
    )
