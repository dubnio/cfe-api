#coding: utf-8
from django.db import models


class Cac(models.Model):
    """ Geographic location of CFEmátic centers """
    id_state = models.IntegerField()
    state = models.CharField(max_length=100)
    id_city = models.IntegerField()
    city = models.CharField(max_length=120)

    name = models.CharField('nombre', max_length=50)
    address = models.CharField('dirección', max_length=250, blank=True)
    address_extra = models.CharField('adicional', max_length=100, blank=True)
    latitude = models.FloatField("Latitud", blank=True, null=True)
    longitude = models.FloatField("Longitud", blank=True, null=True)
    is_cac = models.BooleanField(default=False)
    is_cfematic = models.BooleanField(default=False)
    schedule_days = models.CharField(max_length=80)
    schedule_hours = models.CharField(max_length=100)
    is_active = models.BooleanField(default=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'Cac'

    def __unicode__(self):
        return self.name
