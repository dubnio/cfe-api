#coding: utf-8
from .models import Cac

from rest_framework import viewsets


class CacViewSet(viewsets.ModelViewSet):
    paginate_by = None
    model = Cac
