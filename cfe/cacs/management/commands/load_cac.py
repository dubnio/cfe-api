#coding: utf-8
import os
import csv
from decimal import Decimal
from django.core.management.base import BaseCommand
from cacs.models import Cac

_CSV = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'cac.csv')
ID_EDO = 0
EDO = 1
ID_CIUDAD = 2
CIUDAD = 3
NOMBRE = 4
DIRECCION = 5
ADICIONAL = 6
LATITUD = 7
LONGITUD = 8
CAC = 9
CFEMATICO = 10
HORARIO_DIAS = 11
HORARIO_HORAS = 12


def unicode_csv_reader(utf8_data, **kwargs):
    csv_reader = csv.reader(utf8_data, delimiter=',', quotechar='"', **kwargs)
    for row in csv_reader:
        yield [unicode(cell, 'utf-8') for cell in row]


class Command(BaseCommand):
    help = 'Loads cfematics from csv file'

    def handle(self, *args, **options):
        count = 0
        with open(_CSV, 'rU') as file:
            reader = unicode_csv_reader(file)
            for row in reader:
                try:
                    cac = Cac(id_state=int(row[ID_EDO].strip()),
                              state=row[EDO].strip(),
                              id_city=int(row[ID_CIUDAD].strip()),
                              city=row[CIUDAD].strip(),
                              name=row[NOMBRE].strip(),
                              address=row[DIRECCION].strip(),
                              address_extra=row[ADICIONAL].strip() if row[ADICIONAL].strip() != 'NULL' else '',
                              latitude=Decimal(row[LATITUD].strip()) if row[LATITUD].strip() != '' else None,
                              longitude=Decimal(row[LONGITUD].strip()) if row[LONGITUD].strip() != '' else None,
                              is_cac=True if row[CAC].strip() == '1' else False,
                              is_cfematic=True if row[CFEMATICO].strip() == '1' else False,
                              schedule_days=row[HORARIO_DIAS].strip(),
                              schedule_hours=row[HORARIO_HORAS].strip()
                              )
                    cac.save()
                    count += 1
                except:
                    pass
            print "%s Cac's loaded" % count
