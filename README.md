## RetoMX CFE Django API Demo
* Heroku ready

### How to use locally
Create virtual environment
```sh
mkvirtualenv cfe
```

Install requirements, for development
```sh
pip install -r requirements/development.txt
```

Set 'PROJECT_ENV' environment variable.
* 'development'
* 'production',
* 'staging'

If no 'PROJECT_ENV' especified takes 'development' settings as default

For development environment:
```sh
sudo sh -c 'echo "export PROJECT_ENV=development" >> /etc/profile.d/environment.sh' && source /etc/profile.d/environment.sh
```

Create database and set specificatons in cfe/settings/development.py

Synchronize db and specify an admin user
```sh
python manage.py syncdb
```

Run project
```sh
python manage.py runserver
```

Go to see the api in http://localhost:8000/api
or the admin in http://localhost:8000/admin
Done.

If you want to try the web services, you will need the environment variables WS_PASSWORD and WS_PRIVATE_KEY, provided by the CFE for this prototype.

Then you could try by getting access through shell
```sh
python manage.py shell_plus
>>>from utils import cfews
>>>test = cfews.ObtenerEstados()
>>>response = test.execute()
```

All methods have sufficient default data to test. But you may also see utils/services.txt to get more information.
